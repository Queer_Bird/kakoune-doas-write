# kakoune-doas-write

Fork of kakoune-sudo-write to make it work with doas. I've only tested it on Gentoo but it should work wherever doas can be found. Just put it in your Kakoune autoload directory.

## Authors and acknowledgment
This dude made the original:
https://github.com/occivink/kakoune-sudo-write

## License
For open source projects, say how it is licensed.

## Project status
If you have run out of energy or time for your project, put a note at the top of the README saying that development has slowed down or stopped completely. Someone may choose to fork your project or volunteer to step in as a maintainer or owner, allowing your project to keep going. You can also make an explicit request for maintainers.

